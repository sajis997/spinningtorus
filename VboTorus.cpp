#include "VboTorus.h"


void VboTorus::generateVertices(float *verts,float *norms,float *tex,unsigned int *el,
                                float outerRadius,float innerRadius)
{
    float ringFactor = (float)(TWOPI / rings);
    float sideFactor = (float)(TWOPI / sides);

    int idx = 0, tidx = 0;
    for( int ring = 0; ring <= rings; ring++ )
    {
        float u = ring * ringFactor;
        float cu = cos(u);
        float su = sin(u);

        for( int side = 0; side < sides; side++ )
        {
            float v = side * sideFactor;
            float cv = cos(v);
            float sv = sin(v);
            float r = (outerRadius + innerRadius * cv);
            verts[idx] = r * cu;
            verts[idx + 1] = r * su;
            verts[idx + 2] = innerRadius * sv;
            norms[idx] = cv * cu * r;
            norms[idx + 1] = cv * su * r;
            norms[idx + 2] = sv * r;
            tex[tidx] = (float)(u / TWOPI);
            tex[tidx+1] = (float)(v / TWOPI);
            tidx += 2;
            // Normalize
            float len = sqrt( norms[idx] * norms[idx] +
                              norms[idx+1] * norms[idx+1] +
                              norms[idx+2] * norms[idx+2] );
            norms[idx] /= len;
            norms[idx+1] /= len;
            norms[idx+2] /= len;
            idx += 3;
        }
    }

    idx = 0;
    for( int ring = 0; ring < rings; ring++ )
    {
        int ringStart = ring * sides;
        int nextRingStart = (ring + 1) * sides;

        for( int side = 0; side < sides; side++ )
        {
            int nextSide = (side+1) % sides;
            // The quad
            el[idx] = (ringStart + side);
            el[idx+1] = (nextRingStart + side);
            el[idx+2] = (nextRingStart + nextSide);
            el[idx+3] = ringStart + side;
            el[idx+4] = nextRingStart + nextSide;
            el[idx+5] = (ringStart + nextSide);
            idx += 6;
        }
    }
}


VboTorus::VboTorus(float outerRadius,float innerRadius,int nsides, int nrings):
    rings(nrings),sides(nsides)
{
    faces = sides * rings;

    //calculate the number of vertices we need to consider
    //one extra ring to duplicate first ring
    int nVerts = sides * (rings + 1);

    //allocate memory for vertices,normals, texture coordinates and elements

    //Verts
    float *v = new float[3 * nVerts];

    //Normals
    float *n = new float[3 * nVerts];

    //Tex coords
    float *tex = new float[2 * nVerts];

    //elements
    unsigned int *el = new unsigned int[6 * faces];

    //now generate vertex data
    generateVertices(v,n,tex,el,outerRadius,innerRadius);


    //create and populate buffer objects
    glGenBuffers(4,vboIDs);

    //bind and populate vertex position
    glBindBuffer(GL_ARRAY_BUFFER,vboIDs[0]);
    glBufferData(GL_ARRAY_BUFFER,(3 * nVerts) * sizeof(float),v,GL_STATIC_DRAW);

    //bind and populate vertex normals
    glBindBuffer(GL_ARRAY_BUFFER,vboIDs[1]);
    glBufferData(GL_ARRAY_BUFFER,(3 * nVerts) * sizeof(float),n,GL_STATIC_DRAW);

    //bind and populate vertex texture coordinates
    glBindBuffer(GL_ARRAY_BUFFER,vboIDs[2]);
    glBufferData(GL_ARRAY_BUFFER,(2 * nVerts) * sizeof(float),tex,GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboIDs[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,6 * faces * sizeof(unsigned int),el,GL_STATIC_DRAW);

    delete [] v;
    delete [] n;
    delete [] el;
    delete [] tex;

    //create the vertex array object
    glGenVertexArrays(1,&vaoID);
    glBindVertexArray(vaoID);

    //vertex position
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,vboIDs[0]);
    glVertexAttribPointer((GLuint)0,3,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(0));

    //vertex normal
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER,vboIDs[1]);
    glVertexAttribPointer((GLuint)1,3,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(0));

    //texture coords
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER,vboIDs[2]);
    glVertexAttribPointer((GLuint)2,2,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(0));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboIDs[3]);

    glBindVertexArray(0);

}

VboTorus::~VboTorus()
{
    glDeleteBuffers(4,vboIDs);
    glDeleteVertexArrays(1,&vaoID);
}

void VboTorus::render() const
{
    glBindVertexArray(vaoID);
    //the last argument is in bytes - mentions the offset
    //into the element buffer
    glDrawElements(GL_TRIANGLES,6 * faces,GL_UNSIGNED_INT,BUFFER_OFFSET(0));

}

unsigned int VboTorus::getVaoHandle() const
{
    return vaoID;
}

int VboTorus::getFaces() const
{
    return faces;
}

int VboTorus::getRings() const
{
    return rings;
}

int VboTorus::getSides() const
{
    return sides;
}
