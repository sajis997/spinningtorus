#include "VboTorus.h"

static void error_callback(int error, const char* description);
static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
static void scroll_callback(GLFWwindow* window, double x, double y);

GLFWwindow *window = NULL;
GLSLShader *spinningTorusShader = NULL;
VboTorus *torus = NULL;

int winWidth = 512;
int winHeight = 512;

GLfloat zoom = 22.5f;

glm::mat4 ModelviewMatrix = glm::mat4(1.0f);
glm::mat4 ProjectionMatrix = glm::mat4(1.0f);

//remove the following to draw only one torus
#define MANY_TORUS

void startup();
void shutdown();
void render(float);
void loadShaders();
void loadGeometry();


int main()
{
    // Initialise GLFW before using any of its function
    if( !glfwInit() )
    {
       std::cerr << "Failed to initialize GLFW." << std::endl;
       exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    //we want the opengl 4
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    //we do not want the old opengl
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( winWidth, winHeight, "Spinning Torus Demo", NULL, NULL);

    if( window == NULL )
    {
       fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible. Try the 2.1 version of the tutorials.\n" );

       //we could not initialize the glfw window
       //so we terminate the glfw
       glfwTerminate();
       exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(error_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetScrollCallback(window,scroll_callback);



    //make the current window context current
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwGetFramebufferSize(window,&winWidth,&winHeight);
    framebuffer_size_callback(window,winWidth,winHeight);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile

    if (glewInit() != GLEW_OK)
    {
       fprintf(stderr, "Failed to initialize GLEW\n");
       exit(EXIT_FAILURE);
    }

    if(!glewIsSupported("GL_VERSION_4_4"))
    {
       std::cerr << "OpenGL version 4.4 is yet to be supported. " << std::endl;
       exit(1);
    }

    while(glGetError() != GL_NO_ERROR) {}

    //print out information aout the graphics driver
    std::cout << std::endl;
    std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GLEW Verion: " << glewGetString(GLEW_VERSION) << std::endl;
    std::cout << "OpenGL Vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;


    startup();

    do{
       render(static_cast<float>(glfwGetTime()));
       // Swap buffers
       glfwSwapBuffers(window);
       glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
       glfwWindowShouldClose(window) == 0 );


    //once the following function is called
    //no more events will be dilivered for that
    //window and its handle becomes invalid
    glfwDestroyWindow(window);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    shutdown();

    exit(EXIT_SUCCESS);
}


void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    winWidth = width;
    winHeight = height;

    glViewport(0,0,(GLsizei) winWidth,(GLsizei) winHeight);

    float aspect = (float)winWidth/(float)winHeight;

    ProjectionMatrix = glm::perspective(50.0f,aspect,0.1f,1000.0f);


}

void scroll_callback(GLFWwindow* window, double x, double y)
{
    //increase or decrease field of view based on mouse wheel
    zoom += (float) y / 4.f;
    if (zoom < 0)
       zoom = 0;
}


void startup()
{
    loadShaders();

    //load the torus geometry
    torus = new VboTorus(0.7f,0.3f,30,30);

    //enable depth testing
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);


    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
}

void shutdown()
{
    if(torus)
    {
        delete torus;
        torus = NULL;
    }
}


void render(float time)
{
    static const GLfloat green[] = {0.0f,0.25f,0.0f,1.0f};
    static const GLfloat one = 1.0f;

    glfwGetFramebufferSize(window,&winWidth,&winHeight);
    glViewport(0,0,winWidth,winHeight);

    float aspect = (float)winWidth/(float)winHeight;

    ProjectionMatrix = glm::perspective(50.0f,aspect,0.1f,1000.0f);


    glClearBufferfv(GL_COLOR,0,green);
    glClearBufferfv(GL_DEPTH,0,&one);

#ifdef MANY_TORUS
    for(unsigned int i = 0; i < 24; ++i)
    {
        float f = (float)i + time * 0.3f;

        glm::mat4 trans1 = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.0f,-zoom));
        glm::mat4 rotY = glm::rotate(glm::mat4(1.0f),time ,glm::vec3(0.0f,1.0f,0.0f));
        glm::mat4 rotX = glm::rotate(glm::mat4(1.0f),time ,glm::vec3(1.0f,0.0f,0.0f));
        glm::mat4 trans2 = glm::translate(rotX,glm::vec3(sinf(2.1f * f) * 2.0f,
                                                         cosf(1.7f * f) * 2.0f,
                                                         sinf(1.3f * f) * cosf(1.5f * f) * 2.0f));



        ModelviewMatrix = trans1 * rotY * rotX * trans2;

        spinningTorusShader->Use();


        //send the projection matrix to the shader
        glUniformMatrix4fv((*spinningTorusShader)("ProjectionMatrix"),1,GL_FALSE,glm::value_ptr(ProjectionMatrix));
        glUniformMatrix4fv((*spinningTorusShader)("ModelviewMatrix"),1,GL_FALSE,glm::value_ptr(ModelviewMatrix));



        torus->render();

        spinningTorusShader->UnUse();
    }
#else

    float f = time * 0.3f;

    glm::mat4 rotX = glm::rotate(glm::mat4(1.0f),time,glm::vec3(1.0f,0.0f,0.0f));
    glm::mat4 rotY = glm::rotate(glm::mat4(1.0f),time,glm::vec3(0.0f,1.0f,0.0f));
    glm::mat4 trans1 = glm::translate(glm::mat4(1.0f),glm::vec3(sinf(2.1f * f) * 0.5f,
                                                                cosf(1.7f * f) * 0.5f,
                                                                sinf(1.3f * f) * cosf(1.5f * f) * 2.0f));
    glm::mat4 trans2 = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,0.0f,-14.0f));



    ModelviewMatrix = trans2 * trans1 * rotY * rotX;

    spinningTorusShader->Use();

    //send the projection matrix to the shader
    glUniformMatrix4fv((*spinningTorusShader)("ProjectionMatrix"),1,GL_FALSE,glm::value_ptr(ProjectionMatrix));

    glUniformMatrix4fv((*spinningTorusShader)("ModelviewMatrix"),1,GL_FALSE,glm::value_ptr(ModelviewMatrix));


    torus->render();

    spinningTorusShader->UnUse();

#endif

}


void loadShaders()
{
    if(spinningTorusShader)
    {
        spinningTorusShader->DeleteShaderProgram();
        delete spinningTorusShader;
        spinningTorusShader = NULL;
    }

    spinningTorusShader = new GLSLShader();
    spinningTorusShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/spinningtorus.vert");
    spinningTorusShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/spinningtorus.frag");

    spinningTorusShader->CreateAndLinkProgram();

    spinningTorusShader->Use();
    spinningTorusShader->AddUniform("ModelviewMatrix");
    spinningTorusShader->AddUniform("ProjectionMatrix");
    spinningTorusShader->UnUse();
}
