#ifndef VBOTORUS_H
#define VBOTORUS_H

#include "defines.h"

class VboTorus
{
private:
    unsigned int vaoID;
    unsigned int vboIDs[4];
    int faces, rings, sides;

    void generateVertices(float *,float *,float *,unsigned int *,
                          float,float);
public:

    VboTorus(float,float,int,int);
    ~VboTorus();

    void render() const;

    unsigned int getVaoHandle() const;
    int getFaces() const;
    int getRings() const;
    int getSides() const;
};

#endif // VBOTORUS_H
