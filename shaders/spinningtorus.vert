#version 430 core

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec3 vNormal;
layout (location = 2) in vec2 vTexCoord;


out VS_OUT
{
  vec4 color;
} vs_out;

uniform mat4 ModelviewMatrix;
uniform mat4 ProjectionMatrix;

void main()
{
   gl_Position = ProjectionMatrix * ModelviewMatrix * vec4(vPosition,1.0f);

   vs_out.color = vec4(vPosition,1.0f) * 2.0 + vec4(0.5,0.5,0.5,0.0);
}