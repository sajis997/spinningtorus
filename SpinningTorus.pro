TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += .
INCLUDEPATH += /usr/local/include/GLFW


LIBS += -lglfw -lGLEW -lGL -lGLU -lSOIL

SOURCES += main.cpp \
    GLSLShader.cpp \
    VboTorus.cpp

HEADERS += \
    GLSLShader.h \
    VboTorus.h \
    defines.h

