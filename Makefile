CC = g++
CFLAGS = -O2 -g

INCLUDE = -I/usr/local/include/GLFW

LDFLAGS =  -lglfw -lGLEW -lGL -lGLU
COBJS = $(patsubst %.cpp,%.o,$(wildcard *.cpp))

EXE= SpinningTorus

all: $(COBJS) 
	$(CC) $(CFLAGS) -o $(EXE) $(COBJS) $(LDFLAGS)

%.o : %.cpp 
	$(CC) $(CFLAGS) -o $@ -c $< $(INCLUDE) 


clean:
	rm -f $(EXE) *.o *~

